# Köln | Dialog - Guidelines

***

## HTML / CSS

### HTML5

Use error free html5.

### Responsive design

**Fluid Design**

Always use fluid design that's not device dependent.

**Responsive images**

Responsive images are a must have for header and slider images. Please use [*picturefill.js*](https://github.com/scottjehl/picturefill) for browser compatibility.

Example code:

```htm
<picture>
  <source media="(min-width: 40em)"
    srcset="big.jpg 1x, big-hd.jpg 2x">
  <source 
    srcset="small.jpg 1x, small-hd.jpg 2x">
  <img src="fallback.jpg" alt="">
</picture>
```


### Browser compatibility

Browser compatibility for up to 2 versions below current version.

***

## WordPress

### Core hacks

Core hacks are modifications of the WordPress core **and** modifications of plugin core files. 

Core hacks are **strictly prohibited**. 

Always make sure that the projects are update safe for both WordPress and plugin updates.

### Theme Frameworks

We like theme creation from scratch. In case you're using theme frameworks like *Genesis* please tell us about it!

### Using existing themes as coding basis

Using existing themes (e.g. the standard WP themes, pro themes from Theme Forest, ...) as coding basis is **strictly prohibited**.

### Deprecated functions

Please don't use deprecated WordPress functions. You can find a list of those deprecated functions [here](https://codex.wordpress.org/Category:Deprecated_Functions).

### Hooks and filters

Please use WordPress hooks and filters where possible.

### Multi language projects

For multi language projects please use the plugin [WPML](https://wpml.org/). The plugin will be delivered by us.

### Respect the WordPress coding standards

Please generally respect the [WordPress coding standards](https://make.wordpress.org/core/handbook/best-practices/coding-standards/).

***

## General workflow

The following is optional, but we highly encourage you to use these techniques:

- [**git**](https://git-scm.com/) for version control
- [**sass**](http://sass-lang.com/) or [**less**](http://lesscss.org/) as pre-processors
- [**gulp**](http://gulpjs.com/) or [**grunt**](http://gruntjs.com/) for task automation and project building
- [**bower**](http://bower.io/) as package manager


